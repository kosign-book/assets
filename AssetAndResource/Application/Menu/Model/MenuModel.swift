//
//  MenuModel.swift
//  AssetAndResource
//
//  Created by Navy on 17/10/22.
//

import Foundation

enum MenuRowType {
    case Color
    case Image
    case Data
}

struct MenuModel<T> {
    var value: T?
}

struct MenuInfo {
    var title   : String
    var rowType : MenuRowType
}
