//
//  MenuCell.swift
//  AssetAndResource
//
//  Created by Navy on 17/10/22.
//

import UIKit

class MenuCell: UITableViewCell {
    @IBOutlet weak var containerView : UIView!
    @IBOutlet weak var titleLabel    : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 10
    }
    
}
