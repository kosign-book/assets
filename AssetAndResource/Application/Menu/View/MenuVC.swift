//
//  MenuVC.swift
//  AssetAndResource
//
//  Created by Navy on 17/10/22.
//

import UIKit




class MenuVC: UIViewController {
    

    
    @IBOutlet weak var tableView: UITableView!
    
    
    let menuVM = MenuVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuVM.initData()
    }
}


extension MenuVC: UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuVM.cell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let data = menuVM.cell[indexPath.row].value as! MenuInfo
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.titleLabel.text = data.title
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let data = menuVM.cell[indexPath.row].value as! MenuInfo
        switch data.rowType {
        case .Color:
            let loginVC = UIStoryboard(name: "ColorSB", bundle: nil).instantiateViewController(withIdentifier: "ColorVC") as! ColorVC
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        case .Image:
            let loginVC = UIStoryboard(name: "ImageSB", bundle: nil).instantiateViewController(withIdentifier: "ImageVC") as! ImageVC
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        case .Data:
            let loginVC = UIStoryboard(name: "NSDataAssetSB", bundle: nil).instantiateViewController(withIdentifier: "NSDataAssetVC") as! NSDataAssetVC
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        }
        
       
    }
    
}
