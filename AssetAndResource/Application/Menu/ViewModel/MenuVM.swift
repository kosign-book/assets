//
//  MenuVM.swift
//  AssetAndResource
//
//  Created by Navy on 17/10/22.
//

import Foundation

class MenuVM {
    
    var cell : [MenuModel<Any>] = []
    
    func initData() {
        cell = []
        cell.append(MenuModel<Any>(value: MenuInfo(title: "1. Color", rowType: .Color)))
        cell.append(MenuModel<Any>(value: MenuInfo(title: "2. Image", rowType: .Image)))
        cell.append(MenuModel<Any>(value: MenuInfo(title: "3. Data", rowType: .Data)))
    }
}
