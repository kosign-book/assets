//
//  ImageVC.swift
//  AssetAndResource
//
//  Created by Navy on 19/10/22.
//

import UIKit

enum ImageType {
    case firstImage
    case secondImage
    case thirdImage
    case forthImage
}

class ImageVC: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var displayImageView : UIImageView!
    @IBOutlet weak var firstImage       : UIImageView!
    @IBOutlet weak var secondImage      : UIImageView!
    @IBOutlet weak var thirdImage       : UIImageView!
    @IBOutlet weak var forthImage       : UIImageView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView() {
        self.displayImageView.image = UIImage(named: "empty")
        self.displayImageView.layer.cornerRadius = 18
        self.firstImage.layer.cornerRadius = 8
        self.secondImage.layer.cornerRadius = 8
        self.thirdImage.layer.cornerRadius = 8
        self.forthImage.layer.cornerRadius = 8
    }

    
    // MARK: - IBAction
    @IBAction func firstButtonOnTap(_ sender: UIButton) {
        displayImageView.image = UIImage(named: "background_1")
    }

    @IBAction func secondButtonOnTap(_ sender: UIButton) {
        displayImageView.image = UIImage(named: "background_2")
    }
    
    @IBAction func thirdButtonOnTap(_ sender: UIButton) {
        displayImageView.image = UIImage(named: "background_3")
    }
    
    @IBAction func forthButtonOnTap(_ sender: UIButton) {
        displayImageView.image = UIImage(named: "background_4")
    }
    
}

