//
//  NSDataAssetVM.swift
//  AssetAndResource
//
//  Created by Navy on 8/12/22.
//

import Foundation


class ProductVM {
    
    var cells : [ProductUIModel<Any>] = []
    
    
    func initCell(data: ProductDataModel) {
        
        // clear
        cells = []
        
        // title
        cells.append(ProductUIModel<Any>(value: ProductUIInfo(title: "", rowyType: .Title)))
        
        // item list
        for obj in data.products ?? [] {
            cells.append(ProductUIModel<Any>(value: ProductUIInfo(title: obj.name, image: obj.image, price: obj.price, qty: obj.qty, rowyType: .ItemList)))
        }
        
        // bottom, total amount
        cells.append(ProductUIModel<Any>(value: ProductUIInfo(title: "", price: data.total_amount, rowyType: .TotalAmount)))
    }
}
