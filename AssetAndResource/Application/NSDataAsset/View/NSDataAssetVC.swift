//
//  NSDataAssetVC.swift
//  AssetAndResource
//
//  Created by Navy on 8/12/22.
//

import UIKit

class NSDataAssetVC: UIViewController {

    // MARK: - @IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Variable
    let productVM = ProductVM()
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // get json data from asset
        loadDataFromAsset()
        
        // get json data from normal JSON
        // getDataFromJSONFile()
        
    }
    
    // MARK: - Functions
    // get json data from asset
    func loadDataFromAsset() {
        
        if let asset_data = NSDataAsset(name: "product_data", bundle: Bundle.main) {
            
            // convert ns data asset to json data
            let jsonData = try? JSONSerialization.jsonObject(with: asset_data.data, options: [])
            
            // convert json data as dictionary
            let dataDic = jsonData as? NSDictionary
            
            // convert dictionary to model object
            var item_data : [Product] = []
            if let list = dataDic?.object(forKey: "products") as? Array<Any> {
                for obj in list {
                    let name  = (obj as AnyObject).object(forKey: "name") as? String
                    let price = (obj as AnyObject).object(forKey: "price") as? String
                    let image = (obj as AnyObject).object(forKey: "image") as? String
                    let qty   = (obj as AnyObject).object(forKey: "qty") as? String

                    item_data.append(Product(name: name, price: price, qty: qty, image: image))
                }
            }
            
            // total amount
            let total_amount = dataDic?["total_amount"] as? String

            // all data
            let allData = ProductDataModel(total_amount: total_amount, products: item_data)
            
            productVM.initCell(data: allData)
            tableView.reloadData()
        }
    }
    
    // get json data from normal resource
    
    
    // call fetch data
    func getDataFromJSONFile() {
         if let localData = self.readLocalFile(forName: "Products") {
             self.parse(jsonData: localData)
         }
    }
    
    // load local data
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name, ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        return nil
    }
   
    // decode data
    private func parse(jsonData: Data) {
        do {
            let decodedData = try JSONDecoder().decode(ProductDataModel.self, from: jsonData)
            print(decodedData)
            productVM.initCell(data: decodedData)
            tableView.reloadData()
            
        } catch {
            print("decode error")
        }
    }

}


// MARK: - UITableView
extension NSDataAssetVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productVM.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = productVM.cells[indexPath.row].value as! ProductUIInfo
        
        switch data.rowyType {
        case .Title:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            return cell
            
        case .ItemList:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemListCell", for: indexPath) as! ItemListCell
            cell.configCell(data: data)
            return cell
        case .TotalAmount:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TotalAmountCell", for: indexPath) as! TotalAmountCell
            cell.totalLabel.text = "Total:     \(data.price ?? "$0.00")"
            return cell
            
        default: return UITableViewCell()
            
        }
    }
    
    
}
