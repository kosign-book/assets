//
//  ItemListCell.swift
//  AssetAndResource
//
//  Created by Navy on 8/12/22.
//

import UIKit

class ItemListCell: UITableViewCell {
    
    
    @IBOutlet weak var productImage     : UIImageView!
    @IBOutlet weak var nameLabel        : UILabel!
    @IBOutlet weak var priceLabel       : UILabel!
    @IBOutlet weak var qtyLabel         : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(data: ProductUIInfo) {
        nameLabel.text      = data.title
        priceLabel.text     = data.price
        qtyLabel.text       = "x \(data.qty ?? "")"
        productImage.image  = UIImage(named: data.image ?? "")
    }



}
