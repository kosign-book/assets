//
//  ProductModel.swift
//  AssetAndResource
//
//  Created by Navy on 8/12/22.
//

import Foundation

enum ProductRowType {
    case Title
    case ItemList
    case TotalAmount
}

// ================ UI Model ==================
struct ProductUIModel<T> {
    var value: T?
}

struct ProductUIInfo {
    var title   : String?
    var image   : String?
    var price   : String?
    var qty     : String?
    var rowyType: ProductRowType?
}


// ============== Data Model ================
struct ProductDataModel: Codable {
    var user_name      : String?
    var total_amount   : String?
    var products       : [Product]?
}

struct Product: Codable {
    var name    : String?
    var price   : String?
    var qty     : String?
    var image   : String?
}

