//
//  ColorVC.swift
//  AssetAndResource
//
//  Created by Navy on 13/10/22.
//

import UIKit

enum KSColor: String {
    case KS_Green
    case KS_Yellow
    case KS_Purple
    case KS_Gray
    case KS_Red
}

class ColorVC: UIViewController {
    
    // MARK: - @IBOutlet
    @IBOutlet weak var blankView        : UIView!
    @IBOutlet weak var purpleButton     : UIButton!
    @IBOutlet weak var greenButton      : UIButton!
    @IBOutlet weak var grayButton       : UIButton!
    @IBOutlet weak var yellowButton     : UIButton!
    @IBOutlet weak var redButton        : UIButton!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    // MARK: - Functions
    func changColor(color: KSColor) {
        blankView.backgroundColor = UIColor.init(named: color.rawValue)
    }
    
    func setupView() {
        blankView.layer.cornerRadius    = 8
        purpleButton.layer.cornerRadius = 8
        greenButton.layer.cornerRadius  = 8
        grayButton.layer.cornerRadius   = 8
        yellowButton.layer.cornerRadius = 8
        redButton.layer.cornerRadius    = 8
    }
    
    
    // MARK: - @IBAction
    @IBAction func greenButtonDidTap(_ sender: UIButton) {
        changColor(color: .KS_Green)
    }
    
    @IBAction func purpleButtonDidTap(_ sender: UIButton) {
        changColor(color: .KS_Purple)
    }
    
    @IBAction func grayButtonDidTap(_ sender: UIButton) {
        changColor(color: .KS_Gray)
    }
    
    @IBAction func yellowButtonDidTap(_ sender: UIButton) {
        changColor(color: .KS_Yellow)
    }
    
    @IBAction func redButtonDidTap(_ sender: UIButton) {
        changColor(color: .KS_Red)
    }
    
}
